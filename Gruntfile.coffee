pkg = require './package.json'

###
grunt設定
###
module.exports = (grunt) ->
  #
  # Gruntモジュールを読み込む
  #
  for taskName of pkg.devDependencies
    if taskName.substring(0, 6) == 'grunt-' then grunt.loadNpmTasks taskName
  #
  # grunt.initConfig
  #
  config =
    pkg: grunt.file.readJSON('package.json')
    #
    # grunt-contrib-concat
    #
    concat:
      vendors:
        files:
          '<%= pkg.path.dist %>/common/js/libs/vendor.js': [
            'libs/jquery.imgLoader.min.js'
            'libs/jquery.socialthings.min.js'
            'libs/jquery.rollover.min.js'
            'libs/jquery.easing.1.3.js'
            'libs/easestepper.min.js'
            'libs/jquery.easescroller.min.js'
          ]
      scripts:
        files:
          '<%= pkg.path.dist %>/common/js/common.js': [
            '<%= pkg.path.src %>/js/common.js'
          ]
    #
    # grunt-contrib-uglify
    #
    uglify:
      target:
        files:
          '<%= pkg.path.dist %>/common/js/common.js': [
            '<%= pkg.path.dist %>/common/js/common.js'
          ]
    #
    # grunt-contrib-jshint
    #
    jshint:
      options:
        jshintrc: '.jshintrc'
      source:
        expand: true
        cwd: '<%= pkg.path.src %>'
        src: [
          '**/*.js'
        ]
        filter: 'isFile'
    #
    # grunt-contrib-stylus
    # _付きファイルはパーシャルファイルなのでcssへの出力はしない
    # nibライブラリを使う
    #
    stylus:
      dist:
        options:
          compress: false
          import: ["nib"]
        expand: true
        cwd   : '<%= pkg.path.src %>/styl'
        src   : ['**/!(_)*.styl']
        dest  : '<%= pkg.path.dist %>/common/css'
        ext   : '.css'
    #
    # grunt-contrib-sass
    # _付きファイルはパーシャルファイルなのでcssへの出力はしない
    #
    sass:
      dist:
        options:
          style: 'expanded'
        expand: true
        cwd   : "<%= pkg.path.src %>/scss"
        src   : ['**/!(_)*.scss']
        dest  : "<%= pkg.path.dist %>/common/css"
        ext   : ".css"
    #
    # grunt-bake
    #
    bake:
      options:
        content : '<%= pkg.path.src %>/html/_configs/config.json'
        basePath: '<%= pkg.path.src %>/html'
      source:
        expand: true
        cwd   : '<%= pkg.path.src %>/html'
        src   : ['**/!(_)*.html']
        dest  : '<%= pkg.path.dist %>'
        ext   : '.html'
    #
    # grunt-notify
    # ビルド通知
    #
    notify:
      build:
        options:
          title  : 'ビルド完了'
          message: 'タスクが正常終了しました。'
      watch:
        options:
          title  : '監視開始'
          message: 'ローカルサーバーを起動しました'
    #
    # grunt-contrib-connect
    #
    connect:
      server:
        options:
          port    : 3000
          base    : '<%= pkg.path.dist %>'
          hostname: '0.0.0.0'
          spawn   : false
    #
    # grunt-contrib-watch
    #
    watch:
      options:
        livereload: true
        spawn     : false
      html:
        files: [
          '<%= pkg.path.src %>/**/*.html',
          '<%= pkg.path.src %>/html/*.json'
        ]
        tasks: [
          'bake'
          'notify:build'
        ]
      styl:
        files: ['<%= pkg.path.src %>/**/*.styl']
        tasks: [
          'stylus'
          'notify:build'
        ]
      js:
        files: ['<%= pkg.path.src %>/**/*.js']
        tasks: [
          'jshint'
          'concat:scripts'
          'notify:build'
        ]
  #
  # grunt.initConfig
  #
  grunt.initConfig( config )
  #
  # Default Task
  #
  grunt.registerTask 'default', []
  #
  # Start Task
  #
  grunt.registerTask 'start', [
    'concat:vendors'
    'connect'
    'watch'
  ]
  #
  # Build Task
  #
  grunt.registerTask 'build', [
    'jshint'
    'concat'
    'uglify'
    'bake'
    'stylus'
    'notify:build'
  ]