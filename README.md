# WebSite Template

## 1. npm install

- Stylus か Sass か選択して使わないほうをpackage.jsonから削除

## 2. bowerで必要なライブラリをインストール

例)

```shell
bower install jquery#1.9 --save-dev
```

GitHub からインストールする場合

```shell
bower install git://github.com/foo/bar.git
```

- ライブラリは bower_components から手動で /libs ディレクトリにコピー
- コピーしたあとは Grunt で concat とか copy とか色々実行する
- Gitでは bower.json のみ管理する