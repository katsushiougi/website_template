(function(window, document, $){

  var Hoge = (function(){

    function Hoge() {
      console.log('Hello');
    }

    Hoge.prototype.say = function() {
      console.log('World');
    };

    return Hoge;

  })();

  $(function(){

    var hoge = new Hoge();
    hoge.say();

  });

})(window, document, jQuery);